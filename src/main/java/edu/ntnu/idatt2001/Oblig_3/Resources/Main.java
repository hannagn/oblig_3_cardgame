package edu.ntnu.idatt2001.Oblig_3.Resources;

import edu.ntnu.idatt2001.Oblig_3.Core.DeckOfCards;
import edu.ntnu.idatt2001.Oblig_3.Core.HandOfCards;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;



public class Main extends Application implements EventHandler<ActionEvent> {

    Button button1 = new Button();
    Button button2 = new Button();
    Label label1 = new Label();
    Label label2 = new Label();
    Label label3 = new Label();
    Label label4 = new Label();
    TextField txtField1 = new TextField();
    TextField txtField2 = new TextField();
    TextField txtField3 = new TextField();
    TextField txtField4 = new TextField();
    TextArea txtArea = new TextArea();
    DeckOfCards hand = new DeckOfCards();


    @Override
    public void start(Stage primarystage) throws Exception{

        button1.setText("Deal Hand");
        button1.setOnAction(this);
        button1.setLayoutX(463.0);
        button1.setLayoutY(97.0);
        button1.setPrefHeight(26.0);
        button1.setPrefHeight(78.0);

        button2.setText("Check Hand");
        button2.setOnAction(this);
        button2.setLayoutX(463.0);
        button2.setLayoutY(187.0);
        button2.setPrefHeight(26.0);
        button2.setPrefHeight(78.0);

        label1.setText("Sum of faces:");
        label1.setLayoutX(21.0);
        label1.setLayoutY(268.0);

        label2.setText("Flush:");
        label2.setLayoutX(21.0);
        label2.setLayoutY(340.0);

        label3.setText("Cards of heart:");
        label3.setLayoutX(234.0);
        label3.setLayoutY(268.0);

        label4.setText("Queen of spades:");
        label4.setLayoutX(234.0);
        label4.setLayoutY(340.0);

        txtField1.setPromptText("Ex. H10, H12"); //Cards of heart
        txtField1.setLayoutX(320.0);
        txtField1.setLayoutY(263.0);
        txtField1.setPrefHeight(26.0);
        txtField1.setPrefWidth(110.0);

        txtField2.setPromptText("YES/NO"); //Queen of spades
        txtField2.setLayoutX(336.0);
        txtField2.setLayoutY(336.0);
        txtField2.setPrefHeight(26.0);
        txtField2.setPrefWidth(78.0);

        txtField3.setPromptText("25"); //Sum of faces
        txtField3.setLayoutX(119.0);
        txtField3.setLayoutY(263.0);
        txtField3.setPrefHeight(26.0);
        txtField3.setPrefWidth(70.0);

        txtField4.setPromptText("YES/NO"); //Flush
        txtField4.setLayoutX(60.0);
        txtField4.setLayoutY(335.0);
        txtField4.setPrefHeight(26.0);
        txtField4.setPrefWidth(78.0);

        txtArea.setPromptText("Display of cards");
        txtArea.setLayoutX(34.0);
        txtArea.setLayoutY(31.0);
        txtArea.setPrefHeight(200.0);
        txtArea.setPrefWidth(320.0);

        AnchorPane root = new AnchorPane();
        root.getChildren().add(button1);
        root.getChildren().add(button2);
        root.getChildren().add(label1);
        root.getChildren().add(label2);
        root.getChildren().add(label3);
        root.getChildren().add(label4);
        root.getChildren().add(txtField1);
        root.getChildren().add(txtField2);
        root.getChildren().add(txtField3);
        root.getChildren().add(txtField4);
        root.getChildren().add(txtArea);

        Scene scene = new Scene(root, 800, 600);
        primarystage.setTitle("Card Game");
        primarystage.setScene(scene);
        primarystage.show();



    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(ActionEvent actionEvent) {

        if(actionEvent.getSource() == button1){
            txtArea.setText(hand.dealHand(5).getCards().toString());
        }
        if(actionEvent.getSource() == button2) {
            txtField1.setText(hand.dealHand(5).getCardsOfHeart().toString());

            if (hand.dealHand(5).containsS12()) {
                txtField2.setText("YES");
            } else {
                txtField2.setText("NO");
            }
            txtField3.setText(String.valueOf(hand.dealHand(5).getSum()));
            if(hand.dealHand(5).isFlush()){
                txtField4.setText("YES");
            }else{
                txtField4.setText("NO");
            }
        }
        }
    }
