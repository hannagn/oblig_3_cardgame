package edu.ntnu.idatt2001.Oblig_3.Core;

import java.util.ArrayList;

/**
 * A class that represents a hand of cards, containing different methods
 */
public class HandOfCards {

    private ArrayList<PlayingCard> cards;

    public HandOfCards(ArrayList<PlayingCard> cards){
        if(cards.isEmpty()){
            throw new IllegalArgumentException();
        }
        this.cards = cards;
    }
    public HandOfCards(){

    }

    /**
     * A method who checks the sum of the faces of the cards
     * @return an integer
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }
    public int getSum(){
        int sum;
        sum = cards.stream().mapToInt(PlayingCard::getFace).sum();
        return sum;
    }

    /**
     * A method that checks for cards of type heart
     * @return returns the cards of type heart if any, else returns None
     */
    public String getCardsOfHeart(){
        ArrayList<String> heartCards = new ArrayList<>();
        cards.stream().filter(s -> s.getAsString().startsWith("H")).forEach(heart -> heartCards.add(heart.getAsString()));
        StringBuilder hearts = new StringBuilder();
        for (String s: heartCards){
            hearts.append(s).append(" ");
        }
        if (heartCards.size() == 0){
            hearts.append("None");
        }
        return hearts.toString();
    }

    /**
     * A method who checks for the Queen of spades
     * @return true if the hand contains this, else false
     */
    public boolean containsS12(){
        return cards.stream().anyMatch(s -> s.getAsString().equalsIgnoreCase("S12"));
    }

    /**
     * A method who checks the hand for flush
     * @return returns true if conditions for flush is fulfilled, else false
     */
    public boolean isFlush(){
        int numHearts = (int) cards.stream().filter(s -> s.getSuit() == 'H').count();
        int numSpades = (int) cards.stream().filter(s -> s.getSuit() == 'S').count();
        int numDiamonds = (int) cards.stream().filter(s -> s.getSuit() == 'D').count();
        int numClubs = (int) cards.stream().filter(s -> s.getSuit() == 'C').count();

        return numHearts >= 5 || numSpades >= 5 || numDiamonds >= 5 || numClubs >= 5;
    }
    @Override
    public String toString(){
        StringBuilder display = new StringBuilder();
        display.append("|");
        for (PlayingCard card : cards){
            display.append(card.getAsString()).append("|");
        }
        return display.toString();
    }
}
