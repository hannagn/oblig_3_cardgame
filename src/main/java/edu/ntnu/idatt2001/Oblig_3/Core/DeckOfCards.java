package edu.ntnu.idatt2001.Oblig_3.Core;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Aclass that represents a deck of 52 different cards
 */
public class DeckOfCards {
    private final ArrayList<PlayingCard> playingCards;
    private final char[] suit = { 'S', 'H', 'D', 'C' };//gjør om fra tall 0 -> 3 til symbolet i en for løkke?
    private final int [] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

    public DeckOfCards(ArrayList<PlayingCard> playingCards) {
        this.playingCards = playingCards;
    }

    public DeckOfCards() {
        playingCards = new ArrayList<>(51);
        for(char suits:suit){
            for(int faces:face){
                playingCards.add(new PlayingCard(suits, faces));

            }
        }
    }

   public static int getRandom(int from, int to){
        if (from < to)
            return from + new Random().nextInt(Math.abs(to - from));
            return from - new Random().nextInt(Math.abs(to - from));
    }

    /**
     * a method that deals out n defferent cards
     * @param n the number of cards to be dealt
     * @return returns a hand of different cards
     * @throws IllegalArgumentException
     */
    public HandOfCards dealHand(int n) throws IllegalArgumentException{
        if (n <= 0){
            throw new IllegalArgumentException();
        }
        ArrayList<PlayingCard> playingCardsCopy = new ArrayList<>(playingCards);
        ArrayList<PlayingCard> hand = new ArrayList<>();

        for(int i = 0; i < n; i++ ){
            int randomNumber = getRandom(1, playingCardsCopy.size()-1);
            PlayingCard added = playingCardsCopy.get(randomNumber);

            hand.add(added);
            playingCardsCopy.remove(added);
        }
        return new HandOfCards(hand);
   }
   private ArrayList<Integer> initNumList(int size){
        ArrayList<Integer> numList = new ArrayList<>();
        IntStream stream = IntStream.range(0, size);
        stream.forEach(numList::add);
        return numList;
   }
}

